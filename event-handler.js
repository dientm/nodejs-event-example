const emitter = require('./event-emitter')

function handlers() {
    emitter.on('perimeter', (perimeter) => {
        console.log(`The perimeter of the rectangle is ${ perimeter } cm. `)
    });

    emitter.on('area', (area) => {
        console.log(`The area of the rectangle is ${ area } cm2.`)
    });
}

module.exports = {
    handlers
}