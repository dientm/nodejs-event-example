const { handlers } = require('./event-handler')
const { calculateArea, calculatePerimeter } = require('./calculate')

function main() {
    handlers();

    const LENGTH = 5;
    const WIDTH = 6;

    calculatePerimeter(LENGTH, WIDTH)
    calculateArea(LENGTH, WIDTH)
}

main()