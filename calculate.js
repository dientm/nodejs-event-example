const emitter = require('./event-emitter')

function calculatePerimeter(length, width) {
    const perimeter = 2 * (length + width)
    emitter.emit('perimeter', perimeter);
}

function calculateArea(length, width) {
    const area = length + width;
    emitter.emit('area', area)
}

module.exports = {
    calculateArea,
    calculatePerimeter
}